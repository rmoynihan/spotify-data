##############################################
#                                            #
# 'Billboard Top 100 Over Time' Data Project #
#                                            #
#         Compiled By Riley Moynihan         #
#                                            #
##############################################


###########
# Summary #
###########

This dataset includes over 6,000 'Billboard Hot 100 Singles', dating from 1940 all the way up to 2017.
Each entry comprises:

  -the song's rank for that year
  -the artist(s) names
  -the song's official title
  -the year it was a part of the Billboard Hot 100 (songs can appear multiple times)
  -the song's Spotify ID

as well as the following musical attributes about each track (from the Spotify/Echonest API):

  -danceability
  -energy 
  -key
  -loudness
  -mode
  -speechiness
  -acousticness
  -instrumentalness
  -valence
  -tempo
  -duration
  -time signature
  -difference**

(to see detailed definitions for each attribute, visit Spotify's API guide @ https://developer.spotify.com/web-api/get-audio-features/ )

**: 'Difference' is the only attribute not directly queried from Spotify. This is a calculated field that measures how different
each song's attributes are from combined average of other Hot 100 songs in the same year. Attributes included in this 'difference' are
danceability, energy, speechiness, acousticness, instrumentalness, valence, and difference.


###########
# Process #
###########

All code was written in Python...

Steps:

1. A webscraper was built to gather the original list comprising of the songs, artists, ranks, and year.
2. Some manual data cleaning was needed as the sources for the Hot 100 lists were not all created similarly.
3. With the song list cleaned, a script to search for each song's Spotify Track ID was built.
4. The search-script had about a 90% success rate on it's first fun, so some tweaks were made.
5. After the third search script iteration, 99% of songs had their ID; the remainder were done manually.
6. After all Spotify ID's were gathered, the songs were queried in the API for their extended audio features
7. Finally, a script was made to calculate the 'difference' field using all of the other audio feature fields


###########
# Results #
###########

While I have no means exhausted the potential for interesting research, I have done some cursory statistical analyses with the data:

  -A multiple regression model was created using all of the relevant quantitative predictors to predict the song's 'year'.
This model had an overall high statistical significance (P-value of < .001) and a multiple R^2 value of about 66%. Most predictors
had a similar level of individual significance (but not all). Further tests should be done to come to the most efficient model.

  -A simple regression model using 'difference' to predict a song's year was created. This model, although having a low R^2 value of
about 3%, does have very high statistical significance (P-value of < .001). Additionally, a simple scatterplot of the two variables
shows a clear negative linear correlation between them, indicating that popular songs are becoming more homogenous over time.
