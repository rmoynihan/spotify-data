import spotipy
import spotipy.util as util
import sys
import json
import requests
import csv
import time

def parse_songs():

    #Create CSV reader object

    f = open('1940_2017.csv', 'r')
    fieldnames = [  'rank',
                    'artist',
                    'song',
                    'year',
                    'song_id',
                    'danceability',
                    'energy',
                    'key',
                    'loudness',
                    'mode',
                    'speechiness',
                    'acousticness',
                    'instrumentalness',
                    'valence',
                    'tempo',
                    'duration_sec',
                    'time_signature']
    reader = csv.DictReader(f, fieldnames=fieldnames)

    #Create CSV writer object

    out = open('DIFF_1940_2017.csv', 'w')
    fieldnames = [  'rank',
                    'artist',
                    'song',
                    'year',
                    'song_id',
                    'danceability',
                    'energy',
                    'key',
                    'loudness',
                    'mode',
                    'speechiness',
                    'acousticness',
                    'instrumentalness',
                    'valence',
                    'tempo',
                    'duration_sec',
                    'time_signature',
                    'difference']
                    
    writer = csv.DictWriter(out, fieldnames=fieldnames)

    #Create lists for all fields (used for bulk processing by year)

    rank_list = []
    artist_list = []
    song_list = []
    year_list = []
    song_id_list = []

    dance_list = []
    energy_list = []
    key_list = []
    loudness_list = []
    mode_list = []
    speechiness_list = []
    acousticness_list = []
    instrumentalness_list = []
    valence_list = []
    tempo_list = []
    duration_list = []
    time_signature_list = []

    #Set 'this_year' to first recorded year, will be used to compare when year changes

    this_year = 1940

    #Begin parsing file

    for row in reader:
        year = int(row['year'])

        #Keep collecting information for current year
        if year == this_year:
            
            rank = row['rank']
            rank_list.append(rank)

            artist = row['artist']
            artist_list.append(artist)

            song = row['song']
            song_list.append(song)

            year = int(row['year'])
            year_list.append(year)

            song_id = row['song_id']
            song_id_list.append(song_id)
            
            #Extended Info

            dance = float(row['danceability'])
            dance_list.append(dance)

            energy = float(row['energy'])
            energy_list.append(energy)

            key = row['key']
            key_list.append(key)

            loudness = float(row['loudness'])
            loudness_list.append(loudness)

            mode = row['mode']
            mode_list.append(mode)

            speech = float(row['speechiness'])
            speechiness_list.append(speech)

            acoustic = float(row['acousticness'])
            acousticness_list.append(acoustic)

            instrumental = float(row['instrumentalness'])
            instrumentalness_list.append(instrumental)

            valence = float(row['valence'])
            valence_list.append(valence)

            tempo = float(row['tempo'])
            tempo_list.append(tempo)
    
            duration = float(row['duration_sec'])
            duration_list.append(duration)

            time_sign = row['time_signature']
            time_signature_list.append(time_sign)

        #Year change, begin bulk process
        else:
          
            year_avg = 0
            avg_list = []

            #Calculate yearly averages

            dance_avg = sum(dance_list)/len(dance_list)
            avg_list.append(dance_avg)
            energy_avg = sum(energy_list)/len(energy_list)
            avg_list.append(energy_avg)
            #loudness_avg = sum(loudness_list)/len(loudness_list)
            #avg_list.append(loudness_avg)
            speech_avg = sum(speechiness_list)/len(speechiness_list)
            avg_list.append(speech_avg)
            acoustic_avg = sum(acousticness_list)/len(acousticness_list)
            avg_list.append(acoustic_avg)
            instrumental_avg = sum(instrumentalness_list)/len(instrumentalness_list)
            avg_list.append(instrumental_avg)
            valence_avg = sum(valence_list)/len(valence_list)
            avg_list.append(valence_avg)
            #tempo_avg = sum(tempo_list)/len(tempo_list)
            #avg_list.append(tempo_avg)
            #duration_avg = sum(duration_list)/len(duration_list)
            #avg_list.append(duration_avg)

            year_avg = sum(avg_list)/len(avg_list)

            #Go through all songs in the current year
            for i in range(len(song_id_list)):
                
                rank = rank_list[i]
                artist = artist_list[i]
                song = song_list[i]
                year = year_list[i]
                song_id = song_id_list[i]

                danceability = dance_list[i]
                energy = energy_list[i]
                key = key_list[i]
                loudness = loudness_list[i]
                mode = mode_list[i]
                speechiness = speechiness_list[i]
                acousticness = acousticness_list[i]
                instrumentalness = instrumentalness_list[i]
                valence = valence_list[i]
                tempo = tempo_list[i]
                duration_sec = duration_list[i]
                time_signature = time_signature_list[i]

                #Calculate average for this song

                #song_avg = danceability+energy+loudness+speechiness+acousticness+instrumentalness+valence+tempo+duration_sec 
                song_avg = danceability+energy+speechiness+acousticness+instrumentalness+valence 
                song_avg /= 6

                #Calculate difference field

                difference = abs(year_avg - song_avg)

                #Write results to file

                out_row =   {'rank':rank,
                            'artist':artist,
                            'song':song,
                            'year':year,
                            'song_id':song_id,
                            'danceability':danceability,
                            'energy':energy,
                            'key':key,
                            'loudness':loudness,
                            'mode':mode,
                            'speechiness':speechiness,
                            'acousticness':acousticness,
                            'instrumentalness':instrumentalness,
                            'valence':valence,
                            'tempo':tempo,
                            'duration_sec':duration_sec,
                            'time_signature':time_signature,
                            'difference':difference}
                
                print(out_row)
                writer.writerow(out_row)
                #print(year)

            
            #Reset all lists

            this_year += 1

            rank_list = []
            artist_list = []
            song_list = []
            year_list = []
            song_id_list = []
            
            dance_list = []
            energy_list = []
            key_list = []
            loudness_list = []
            mode_list = []
            speechiness_list = []
            acousticness_list = []
            instrumentalness_list = []
            valence_list = []
            tempo_list = []
            duration_list = []
            time_signature_list = []



    print('\n\nJob Completed.\n')

    f.close()
    out.close()


#Call method
parse_songs()
