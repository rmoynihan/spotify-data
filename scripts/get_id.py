import spotipy
import spotipy.util as util
import sys
import json
import requests
import csv
import time

#Method to search for a single song's Spotify ID based on its title and artist
def get_song_id(artist, song):

    #Create authorization information

    scope = 'user-library-read'
    username = YOURUSERNAMEHERE
    c_id = YOURCLIENTIDHERE
    c_secret = YOURCLIENTSECRETHERE
    token = util.prompt_for_user_token(username, scope, client_id=c_id, client_secret=c_secret, redirect_uri='http://google.com/')

    if token:

        #Create API request    

        base_url = 'https://api.spotify.com/v1/search'
        headers={ 'Authorization':'Bearer ' + token }
        params={'q':'track:' + song + ' artist:' + artist, 'type':'track'}
        r = requests.get(base_url, headers=headers, params=params)

        response = json.loads(r.content)

        #Get ID from reponse

        try:
            #print(response['tracks']['items'][0]['artists'][0]['name'])
            #print(response['tracks']['items'][0]['name'])
            #print(response['tracks']['items'][0]['id'])
            song_id = response['tracks']['items'][0]['id']
            return(str(song_id))

        except IndexError:
            #print(json.dumps(response, indent=4))
            return('error')

        except KeyError:
            return('error')

    else:
        print("SEARCH ERROR")
        print("Can't get token for", username)


#Goes through song list and searches for ID
def parse_songs():
    f = open('1940_2017.csv', 'r')
    fieldnames = ['rank', 'artist', 'song', 'year']
    reader = csv.DictReader(f, fieldnames=fieldnames)

    out = open('ID_1940_2017.csv', 'w')
    fieldnames = ['rank', 'artist', 'song', 'year', 'song_id']
    writer = csv.DictWriter(out, fieldnames=fieldnames)

    error_count = []

    for row in reader:
        rank = row['rank']
        artist = row['artist']
        song = row['song']
        year = row['year']

        print('Searching for', song, 'by', artist, ' - ', year)

        #If multiple artists, this is the notation I chose and now must remove
        artist_list = []
        if '/' in artist:
            cmb_artist = artist

            while '/' in cmb_artist:
                artist_list.append(cmb_artist.split('/', 1)[0])
                cmb_artist = cmb_artist.split('/', 1)[1]
            
            count = 1
            for a in artist_list:
                
                print('\n\n*** Sub-Search ***\n')
                print('artist:', a)
                
                song_id = get_song_id(a, song)

                if song_id == 'error':
                    if count == len(artist_list):
                        error_count.append((a, song, year))            
                        print("No song found...\n\n")
                        
                        song_id = ''
                        out_row = {'rank':rank, 'artist':artist, 'song':song, 'year':year, 'song_id':song_id}
                        writer.writerow(out_row)

                    count += 1

                else:
                    out_row = {'rank':rank, 'artist':a, 'song':song, 'year':year, 'song_id':song_id}
                    writer.writerow(out_row)
                    print("Song found!\n\n")
                    break

                time.sleep(2)

        #If single artist
        else:
            song_id = get_song_id(artist, song)

            if song_id == 'error':
                error_count.append((artist, song, year))
                print("No song found...\n\n")
                
                song_id = ''
                out_row = {'rank':rank, 'artist':artist, 'song':song, 'year':year, 'song_id':song_id}
                writer.writerow(out_row)

            else:
                out_row = {'rank':rank, 'artist':artist, 'song':song, 'year':year, 'song_id':song_id}
                writer.writerow(out_row)
                print("Song found!\n\n")

        time.sleep(2)

    print('\n\nJob Completed with ', len(error_count), 'errors.')

    f.close()
    out.close()


parse_songs()
