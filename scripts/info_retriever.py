import spotipy
import spotipy.util as util
import sys
import json
import requests
import csv
import time

def get_info(songs):

    scope = 'user-library-read'
    username = 'pilot2016'
    c_id = 'f803bd4b2e5e4ea09f0c7dd59636ae85'
    c_secret = '1e17c9de88f4468a8b1b191ca3dbdeae'
    token = util.prompt_for_user_token(username, scope, client_id=c_id, client_secret=c_secret, redirect_uri='http://google.com/')

    if token:

        sp = spotipy.Spotify(auth=token)
        features = sp.audio_features(songs)
        return features

        ###################
        # Non-Spotipy Way #
        ###################

        #base_url = 'https://api.spotify.com/v1/search'
        #headers={ 'Authorization':'Bearer ' + token }
        #params={'q':'track:' + song + ' artist:' + artist, 'type':'track'}
        #params={'q':'track:Blood On The Radio artist:Thank You Scientist', 'type':'track'}
        #r = requests.get(base_url, headers=headers, params=params)

        #response = json.loads(r.content)
        #try:
            #print(response['tracks']['items'][0]['artists'][0]['name'])
            #print(response['tracks']['items'][0]['name'])
            #print(response['tracks']['items'][0]['id'])
            #song_id = response['tracks']['items'][0]['id']
            #return(str(song_id))

        #except IndexError:
            #print(json.dumps(response, indent=4))
            #return('error')

        #except KeyError:
            #return('error')

    else:
        print("SEARCH ERROR")
        print("Can't get token for", username)


def parse_songs():
    f = open('2017.csv', 'r')
    fieldnames = ['rank', 'artist', 'song', 'year', 'song_id']
    reader = csv.DictReader(f, fieldnames=fieldnames)

    out = open('FEATURES_2017.csv', 'w')
    fieldnames = [  'rank',
                    'artist',
                    'song',
                    'year',
                    'song_id',
                    'danceability',
                    'energy',
                    'key',
                    'loudness',
                    'mode',
                    'speechiness',
                    'acousticness',
                    'instrumentalness',
                    'valence',
                    'tempo',
                    'duration_sec',
                    'time_signature']
                    
    writer = csv.DictWriter(out, fieldnames=fieldnames)

    rank_list = []
    artist_list = []
    song_list = []
    year_list = []
    song_id_list = []

    count = -1
    error_count = 0
    for row in reader:
        count += 1
        if count < 94:
            rank = row['rank']
            rank_list.append(rank)

            artist = row['artist']
            artist_list.append(artist)

            song = row['song']
            song_list.append(song)

            year = row['year']
            year_list.append(year)

            song_id = row['song_id']
            song_id_list.append(song_id)

        else:
            features = get_info(song_id_list)
           
            for i in range(len(features)):
                rank = rank_list[i]
                artist = artist_list[i]
                song = song_list[i]
                year = year_list[i]
                song_id = song_id_list[i]

                sf = features[i]

                try:
                    danceability = sf['danceability']
                    energy = sf['energy']
                    key = sf['key']
                    loudness = sf['loudness']
                    mode = sf['mode']
                    speechiness = sf['speechiness']
                    acousticness = sf['acousticness']
                    instrumentalness = sf['instrumentalness']
                    valence = sf['valence']
                    tempo = sf['tempo']
                    duration_sec = float(int(sf['duration_ms'])*.001)
                    time_signature = sf['time_signature']

                    out_row =   {'rank':rank,
                                'artist':artist,
                                'song':song,
                                'year':year,
                                'song_id':song_id,
                                'danceability':danceability,
                                'energy':energy,
                                'key':key,
                                'loudness':loudness,
                                'mode':mode,
                                'speechiness':speechiness,
                                'acousticness':acousticness,
                                'instrumentalness':instrumentalness,
                                'valence':valence,
                                'tempo':tempo,
                                'duration_sec':duration_sec,
                                'time_signature':time_signature}
                    
                    writer.writerow(out_row)
                    print(year)
                    #print(json.dumps(out_row, indent=4))
                
                except TypeError:
                    print('\n\n***** Error!!! *****\n')
                    print('Artist: ', artist)
                    print('Song: ', song)
                    print('ID: ', song_id)
                    error_count += 1
                    


            count = 0
            rank_list = []
            artist_list = []
            song_list = []
            year_list = []
            song_id_list = []

            time.sleep(2)


    print('\n\nJob Completed with', error_count, 'errors.\n')

    f.close()
    out.close()


parse_songs()
