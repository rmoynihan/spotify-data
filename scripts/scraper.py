import requests
import re
import time
import csv
import subprocess as sp


headers = {
'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
'accept-encoding':'gzip, deflate, br',
'accept-language':'en-US,en;q=0.8',
'cache-control':'max-age=0'
}

base_url = 'http://billboardtop100of.com/'
url = ''

years = list(range(1950, 2017))


f = open('_songs.csv', 'w')
fieldnames = ['artist', 'song', 'year']
writer = csv.DictWriter(f, fieldnames=fieldnames)

for year in years:
    print(year)
    url = base_url + str(year) + '-2/'

    r = requests.get(url, headers=headers)
    time.sleep(2)

    line_list = []    
    for line in r.text.splitlines():

        line = line.strip()
        line = re.sub('&#[0-9][0-9][0-9][0-9];', "'", line)
        line = line.replace("amp;", '')

        if '/td' in line:
            line_list.append(line)

    for n in range(0,len(line_list)-4,3):
        #print("Rank line: ", line_list[n])
        #print("Arist line: ", line_list[n+1])
        #print("Song line: ", line_list[n+2])

        artist = line_list[n+1]
        if 'id""rec=' in artist or 'id"rec=' in artist:
            artist = artist.split('"">', 1)[1]
            artist = artist.split('<', 1)[0]
            
            artist = artist.split('"">', 1)[1]
            artist = artist.split('<', 1)[0]
            artist = re.sub('[^0-9a-zA-Z]+', '', artist)

        song = line_list[n+2]
        song = song.split('>', 1)[1]
        song = song.split('<', 1)[0]
        
        writer.writerow({'artist':artist, 'song':song, 'year':year})


f.close()


f = open('_songs.csv', 'r')
fieldnames = ['artist', 'song', 'year']
reader = csv.DictReader(f, fieldnames=fieldnames)

out = open('songs.csv', 'w')
writer = csv.DictWriter(out, fieldnames=fieldnames)

for row in reader:
    artist = str(row['artist'])
    #artist = artist.split('"">', 1)[1]
    #artist = artist.split('<', 1)[0]
    artist = re.sub('[^0-9a-zA-Z ]+', '', artist)
    artist = re.sub('[0-9]+', '', artist)
    artist = re.sub('td idrec', '', artist)
    artist = re.sub('td', '', artist)
    
    print(artist)

    writer.writerow({'artist':artist, 'song':row['song'], 'year':row['year']})

f.close()
out.close()

sp.call(['rm', '_songs.csv'])
